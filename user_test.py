import unittest

from user import User


class UserTest(unittest.TestCase):
    def test_user_has_role_user(self):
        user = User()
        user.username = 'eiei'
        user.roles = ['user']
        roles = ['user']
        value = user.has_roles(roles)
        expected = True
        self.assertEqual(value, expected)

    def test_user_not_admin_should_return_false(self):
        user = User()
        user.username = 'eiei'
        user.roles = ['user']
        roles = ['admin']
        value = user.has_roles(roles)
        expected = False
        self.assertEqual(value, expected)

    def test_user_has_role_user_and_admin(self):
        user = User()
        user.username = 'eiei'
        user.roles = ['user', 'admin']
        roles = ['user', 'admin']
        value = user.has_roles(roles)
        expected = True
        self.assertEqual(value, expected)

    def test_user_role_is_empty_should_return_false(self):
        user = User()
        user.username = 'eiei'
        user.roles = []
        roles = ['user']
        value = user.has_roles(roles)
        expected = False
        self.assertEqual(value, expected)


if __name__ == '__main__':
    unittest.main()
