import unittest
from unittest.mock import patch, MagicMock

import oauth2_prod


class OAuthTestFetchToken(unittest.TestCase):

    @patch('oauth2_prod.models')
    def test_give_name_should_get_right_token(self, models_mock):
        obj_mock = MagicMock()
        obj_mock.name = 'Phudit'

        models_mock.OAuth2Token.objects().first.return_value = obj_mock
        res = oauth2_prod.fetch_token('Phudit')

        expected = obj_mock

        self.assertEqual(res, expected)


if __name__ == '__main__':
    unittest.main()
