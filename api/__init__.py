from flask import Flask
from flask_restplus import Api, fields, Resource

import models

restapi = Api(doc='/api')

assessment_model = restapi.model('Assessment', {
    'id': fields.String,
})


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        # a default secret that should be overridden by instance config
        SECRET_KEY="dev",
    )
    restapi.init_app(app)

    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.update(test_config)

    @restapi.route('/api/assessments')
    class Assessment(Resource):
        @restapi.marshal_with(assessment_model)
        def get(self):
            assessments = models.Assessment.objects()
            data = []
            for a in assessments:
                data.append(dict(id=a['id']))
            return data

        @restapi.marshal_with(assessment_model)
        def post(self):
            user = models.User.get_user_by_api_key()

            assessment = models.Assessment(user=user)
            # assessment.save()

            return assessment

    return app
