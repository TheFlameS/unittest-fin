from flask_restplus import Resource, fields
from . import restapi

from wecareyou import models

import datetime


assessment_model = restapi.model('Assessment', {
    'id': fields.String,
})


watchfulness_model = restapi.model('WatchfulnessAssessment', {
    'watchfulness': fields.String,
    'situation': fields.String,
    'created_date': fields.DateTime(dt_format='rfc822'),
    'updated_date': fields.DateTime(dt_format='rfc822'),
    'status': fields.String
})


@restapi.route('/api/assessments')
class Assessment(Resource):
    @restapi.marshal_with(assessment_model)
    def get(self):
        assessments = models.Assessment.objects()
        return list(assessments)

    @restapi.marshal_with(assessment_model)
    def post(self):
        user = models.User.get_user_by_api_key()

        assessment = models.Assessment(user=user)
        assessment.save()

        return assessment


@restapi.route('/api/assessments/<string:assessment_id>/watchfulness')
class WatchfulnessAssessment(Resource):
    @restapi.marshal_with(watchfulness_model)
    def get(self, assessment_id):

        user = models.User.get_user_by_api_key()
        assessment = models.Assessment.objects().get(
                id=assessment_id,
                user=user)
        return assessment.watchfulness

    @restapi.expect(watchfulness_model)
    @restapi.marshal_with(watchfulness_model)
    def post(self, assessment_id):

        user = models.User.get_user_by_api_key()
        if not user:
            return '{}', 500

        assessment = models.Assessment.objects.get(user=user, id=assessment_id)

        payload = restapi.payload

        print(payload)
        assessment.watchfulness.situation = payload.get('situation', None)
        assessment.watchfulness.watchfulness = payload.get('watchfulness', None)
        assessment.watchfulness.updated_date = datetime.datetime.now()

        if assessment.watchfulness.status != 'completed':
            assessment.watchfulness.created_date = datetime.datetime.now()
            assessment.watchfulness.status = 'completed'

        assessment.save()

        return assessment.watchfulness
