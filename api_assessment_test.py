import unittest
from unittest.mock import patch

import api


class ApiTest(unittest.TestCase):

    def setUp(self):
        app = api.create_app()
        app.config['TESTING'] = True

        self.client = app.test_client()
        self.app = app

    def tearDown(self):
        pass

    @patch('api.models')
    def test_assessments_get_should_return_assessment_id(self, api_models_mock):
        mock_return_value = [{'id': '1'}, {'id': '2'}]
        api_models_mock.Assessment.objects.return_value = mock_return_value
        e_1 = {'id': '1'}
        e_2 = {'id': '2'}
        response = self.client.get('/api/assessments')
        # print('case 1 --->', response.get_json())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json()[0], e_1)
        self.assertEqual(response.get_json()[1], e_2)

    @patch('api.models')
    def test_assessments_get_alter_format_should_get_id_only(self, api_models_mock):
        mock_return_value = [{'id': '1', 'name': 'eiei'},
                             {'id': '2', 'name': 'eiei2'}]
        api_models_mock.Assessment.objects.return_value = mock_return_value
        e_1 = {'id': '1'}
        e_2 = {'id': '2'}
        response = self.client.get('/api/assessments')
        # print('case 3 --->', response.get_json())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json()[0], e_1)
        self.assertEqual(response.get_json()[1], e_2)

    @patch('api.models')
    def test_assessments_post_should_get_new_assessment(self, api_models_mock):
        get_api_key_mock = 'api-key-test'
        assessment_mock = {'id': '3'}

        api_models_mock.User.get_user_by_api_key.return_value = get_api_key_mock
        api_models_mock.Assessment.return_value = assessment_mock

        e = '3'

        response = self.client.post('/api/assessments')
        # print("case 2 --->", response.get_json())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json()['id'], e)


if __name__ == '__main__':
    unittest.main()
