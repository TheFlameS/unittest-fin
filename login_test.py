import unittest
from unittest.mock import patch

import views
import json


class BasicTests(unittest.TestCase):

    def setUp(self):
        app = views.create_app()
        app.config['TESTING'] = True

        self.client = app.test_client()
        self.app = app

    def tearDown(self):
        pass

    def test_login_with_not_validate_on_submit_should_redirect_to_login_page(self):
        response = self.client.get('/login', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        # print(data)
        self.assertIn('เข้าสู่ระบบ', data)

    @patch('views.current_user')
    def test_login_with_user_was_authenticated_should_redirect_to_index(self, current_user_mock):
        current_user_mock.is_authenticated.return_value = True
        response = self.client.get('/login', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        # print(data)
        self.assertIn('index', data)

    @patch('views.check_password_hash')
    @patch('views.models')
    @patch('views.forms')
    def test_login_with_incorrect_password_should_redirect_to_login_page(self, forms_mock, models_mock, cph_mock):
        forms_mock.LoginForm.validate_on_submit.return_value = True
        json_data = {'username': 'phudit', 'password_hash': 'wrong'}
        models_mock.User.objects.first.return_value = json.dumps(json_data)
        cph_mock.return_value = False
        response = self.client.get('/login', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        # print(data)
        self.assertIn('เข้าสู่ระบบ', data)

    @patch('views.check_password_hash')
    @patch('views.models')
    @patch('views.forms')
    def test_login_with_no_user_should_redirect_to_login_page(self, forms_mock, models_mock, cph_mock):
        forms_mock.LoginForm.validate_on_submit.return_value = True
        models_mock.User.objects.first.return_value = None
        cph_mock.return_value = False
        response = self.client.get('/login', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        # print(data)
        self.assertIn('เข้าสู่ระบบ', data)


if __name__ == "__main__":
    unittest.main()
