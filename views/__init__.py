from flask import Flask
from flask import (render_template,
                   url_for,
                   redirect)
from flask_login import LoginManager, login_user, current_user

from werkzeug.security import check_password_hash

from . import forms

import models

login_manager = LoginManager()


def init_login_manager(app):
    login_manager.init_app(app)
    login_manager.login_view = 'accounts.login'


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        # a default secret that should be overridden by instance config
        SECRET_KEY="dev",
    )
    init_login_manager(app)

    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.update(test_config)

    @app.route("/index")
    def index():
        return render_template('index.html')

    @app.route("/login")
    def login():
        if current_user.is_authenticated:
            return redirect(url_for('index'))
        form = forms.LoginForm()
        if not form.validate_on_submit():
            return render_template('login.html',
                                   form=form)
        user = models.User.objects(username=form.username.data).first()
        if user:
            if check_password_hash(user.password, form.password.data):
                login_user(user)
            else:
                error = "Wrong password"
                return render_template('login.html',
                                       form=form, pass_error=error)
        else:
            error = "username not found"
            return render_template('login.html',
                                   form=form, error=error)

        return redirect(url_for('index'))

    return app
