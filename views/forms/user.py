from flask_wtf import FlaskForm
from wtforms import fields
from wtforms import validators


class LoginForm(FlaskForm):
    username = fields.TextField('Username',
                                validators=[validators.InputRequired()])
    password = fields.PasswordField('Password',
                                    validators=[validators.InputRequired()])
