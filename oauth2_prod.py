from flask_login import current_user

import models


def fetch_token(name):
    token = models.OAuth2Token.objects(name=name,
                                       user=current_user._get_current_object()).first()
    return token
